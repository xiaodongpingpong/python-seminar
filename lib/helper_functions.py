#!/usr/bin/python
#################################################################################
# FILENAME   : helper_functions.py                                              #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 10/08/2012                                                       #
# DESCRIPTION:                                                                  #
#################################################################################

def add_intercept( data ):
    """
    Add column of ones to matrix for intercept.
    """
    from numpy import matrix, ones, hstack
    ONE = matrix( ones( data.shape[0] ) ).T
    X = hstack( (ONE,  data) )
    return X



def matrix2csv( mat, file ):
    """
    Writes the data in a matrix to a csv file.

    Args:
        mat (matrix) : name of matrix to write
        file (str)   : name of output csv file
    Returns:
        None
    Raises:
        IOError if specified file cannot be opened
    """

    try:
        csv = open( file, "w" )

    except IOError, e:
        print e
        import sys
        sys.exit()

    for i in range( mat.shape[0] ):
        for j in range( mat.shape[1] ):

            if j < mat.shape[1] - 1:
                csv.write( str(mat[i,j]) + "," )
            else:
                csv.write(str( mat[i,j]) + "\n" )

    csv.close()

## END OF FILE
